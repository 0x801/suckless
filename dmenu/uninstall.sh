#!/bin/bash

removeConfig()
{
	rm -f config.h
}

DIR=$(dirname "$0")
cd $DIR

removeConfig
sudo make clean uninstall
removeConfig


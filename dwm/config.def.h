/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>

static const unsigned int scale = 1; // x1 is 1080p

/* appearance */
// border pixel
static const unsigned int borderpx = 2 * scale;		/* border pixel of windows */
// snap pixel
static const unsigned int snap      = 8 * scale;	/* snap pixel */
// systray
static const unsigned int systraypinning = 0;		/* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft = 0;		/* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 2;		/* systray spacing */
static const int systraypinningfailfirst = 1;		/* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;			/* 0 means no systray */
// gaps
static const unsigned int GAP_AMT   = 4 * scale;
static const unsigned int gappih    = GAP_AMT;		/* horiz inner gap between windows */
static const unsigned int gappiv    = GAP_AMT;		/* vert inner gap between windows */
static const unsigned int gappoh    = GAP_AMT;		/* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = GAP_AMT;		/* vert outer gap between windows and screen edge */
static const int smartgaps = 0;						/* 1 means no outer gap when there is only one window */
// top bar
static const int showbar            = 1;			/* 0 means no bar */
static const int topbar             = 1;			/* 0 means bottom bar */
// fonts
static const char *fonts[]          = {
	"Source Code Pro:size=10",
	// 1x scale
	"Font Awesome 6 Brands Regular:pixelsize=12",
	"Font Awesome 6 Free Regular:pixelsize=12",
	"Font Awesome 6 Free Solid:pixelsize=12",
	"Font Awesome v4 Compatibility Regular:pixelsize=12",
	// 2x scale
//	"Font Awesome 6 Brands Regular:pixelsize=24",
//	"Font Awesome 6 Free Regular:pixelsize=24",
//	"Font Awesome 6 Free Solid:pixelsize=24",
//	"Font Awesome v4 Compatibility Regular:pixelsize=24",
};
static const char dmenufont[]       = "Source Code Pro:size=10";
// colors
static const char col_gray1[]       = "#2e2e2e";	/* background color */
static const char col_gray2[]       = "#f2f2f2";	/* inactive window border color */
static const char col_gray3[]       = "#f2f2f2";	/* font color */
static const char col_gray4[]       = "#f2f2f2";	/* current tag color & current window font color */
static const char col_cyan[]        = "#634cf8";	/* top bar secondary color & active window border color */
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
//static const char *tags[] = {
//	"₁",  // 1
//	"₂",  // 2
//	"₃",  // 3
//	"₄",  // 4
//	"₅",  // 5
//	"₆",  // 6
//	"₇",  // 7
//	"₈",  // 8
//	"₉",  // 9
//};

#define TAG(x) (1 << (x-1))
static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class                  instance               title                           tags mask   isfloating   monitor */
/* current monitor */
	{ "Bitwarden",            NULL,                  NULL,                           0,           1,           -1 },
	{ "gnome-calculator",     NULL,                  NULL,                           0,           1,           -1 },
	{ "Rofi",                 "rofi",                NULL,                           0,           1,           -1 },
	{ "Timeshift",            NULL,                  NULL,                           0,           1,           -1 },
	{ "VirtualBox Manager",   NULL,                  NULL,                           TAG(9),      1,           -1 },
	{ "VirtualBox Machine",   NULL,                  NULL,                           TAG(9),      0,           -1 },
	{ "VirtualBoxVM",         NULL,                  "Network Operations Manager",   TAG(9),      1,           -1 },
/* primary monitor */
	// Steam
	{ "Steam",                NULL,                  NULL,                           TAG(4),      1,            0 },  // pop-up windows
	{ "Steam",                NULL,                  "Steam",                        TAG(4),      0,            0 },  // main window
/* secondary monitor */
	{ "discord",              NULL,                  NULL,                           TAG(1),      0,            1 },
	{ "EasyEffects",          NULL,                  NULL,                           0,           1,           -1 },
	{ "Pavucontrol",          NULL,                  NULL,                           0,           1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"
static const Layout layouts[] = {
	// first entry in array is default
	/* symbol     arrange function */
	{ "[]",      tile                   }, // []=
	{ "[]",      spiral                 }, // [@]
	{ "[]",      dwindle                }, // [\\]
 	{ "[M]",      monocle                },
	{ "||",      centeredmaster         }, // |M|
	{ "||",      grid                   }, // HHH
	{ "||",      nrowgrid               }, // ###
	{ "||",      NULL                   }, // ><>
	{ NULL,       NULL                   },

	// The following have been commented out because either I didn't like it or I thought it was redundant.
	// They can be added back by uncommenting here, dwm.c, *AND* vanitygaps.c
	// Note that for the *.c files, you must uncomment the delcaration and implementation
//	{ "H[]",      deck                   },
//	{ "===",      bstackhoriz            },
//	{ "---",      horizgrid              },
//	{ ":::",      gaplessgrid            },
//	{ ">M>",      centeredfloatingmaster },
//	{ "TTT",      bstack                 },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                  KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|Mod1Mask,         KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,        KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,      KEY,      toggletag,      {.ui = 1 << TAG} },
#define MEDIAPLAY XF86XK_AudioPlay
#define MEDIANEXT XF86XK_AudioNext
#define MEDIAPREV XF86XK_AudioPrev
#define BRIGHTUP   XF86XK_MonBrightnessUp
#define BRIGHTDOWN XF86XK_MonBrightnessDown

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]      = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *runnercmd[]     = { "rofi", "-combi-modi", "window,drun,ssh", "-show", "combi", "-icon-theme", "\"Papirus\"", "-show-icons", NULL };
static const char *termcmd[]       = { "alacritty", NULL };
static const char *lockcmd[]       = { "i3lock-fancy", NULL };
static const char *screencapcmd[]  = { "spectacle", NULL };
static const char *fmcmd[]         = { "thunar", NULL };
//static const char *volupcmd[]      = { "amixer", "sset", "Master", "5%+", NULL };
//static const char *voldowncmd[]    = { "amixer", "sset", "Master", "5%-", NULL };
//static const char *voltogglecmd[]  = { "amixer", "sset", "Master", "toggle", NULL };
static const char *mediaplaycmd[]  = { "playerctl", "play-pause", "NULL" };
static const char *medianextcmd[]  = { "playerctl", "next", NULL };
static const char *mediaprevcmd[]  = { "playerctl", "previous", NULL };
static const char *brightupcmd[]   = { "brightnessctl", "set", "5%+", NULL };
static const char *brightdowncmd[] = { "brightnessctl", "set", "5%-", NULL };
static const char *browsercmd[]    = { "firefox", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument              description */

	// Basic keybinds
//	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },   // open dmenu
	{ MODKEY,                       XK_p,      spawn,          {.v = runnercmd } },  // open runner
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },    // open new terminal window
	{ MODKEY,                       XK_b,      togglebar,      {0} },                // toggle dmenu visibility

	// Change focused window on current monitor
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },         // move focus to next tile in stack
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },         // move focus to prev tile in stack

	// Change number of windows in master and slave areas
	{ MODKEY,                       XK_equal,  incnmaster,     {.i = +1 } },         // move tile on top of stack to master side
	{ MODKEY,                       XK_minus,  incnmaster,     {.i = -1 } },         // move tile at bottom of stack to slave side

	// Change position of master/slave divider on current monitor
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },       // shift divider towards master area (ie, shrink master)
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },       // shift divider away from master area (ie, enlarge master)

	// Toggle window between master and slave areas on current monitor
	{ MODKEY,                       XK_m,      zoom,           {0} },                // set focused tile to master
	{ MODKEY,                       XK_Tab,    view,           {0} },                // go to previous tag

	// Kill tile in focus
//	{ Mod1Mask,                     XK_F4,     killclient,     {0} },                // kill focused tile
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },                // kill focused tile
	
	// Lock screen
	{ MODKEY|ShiftMask,             XK_l,      spawn,          {.v = lockcmd } },    // lock the screen

	// Set layout
	{ MODKEY,                       XK_F1,     setlayout,      {.v = &layouts[0]}  },
	{ MODKEY,                       XK_F2,     setlayout,      {.v = &layouts[1]}  },
	{ MODKEY,                       XK_F3,     setlayout,      {.v = &layouts[2]}  },
	{ MODKEY,                       XK_F4,     setlayout,      {.v = &layouts[3]}  },
	{ MODKEY,                       XK_F5,     setlayout,      {.v = &layouts[4]}  },
	{ MODKEY,                       XK_F6,     setlayout,      {.v = &layouts[5]}  },
	{ MODKEY,                       XK_F7,     setlayout,      {.v = &layouts[6]}  },
	{ MODKEY,                       XK_F8,     setlayout,      {.v = &layouts[7]}  },

	// Change layout
	{ MODKEY,                       XK_space,  setlayout,      {0} },                // go to previous layout
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },                // toggle floating mode on focused tile
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },        // view all tiles on all tags from current monitor
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },        // set focused tile to show on all monitors

	// Focus monitor
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },         // move focus to leftward monitor
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },         // move focus to rightward monitor

	// Move tile between monitors
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },         // move focused tile to leftward monitor
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },         // move focused tile to rightward monitor

	// Change gap size (from patch: vanity gaps)
	{ MODKEY|Mod1Mask,              XK_equal,  incrgaps,       {.i = +1 } },         // increment gaps by 1px
	{ MODKEY|Mod1Mask,              XK_minus,  incrgaps,       {.i = -1 } },         // decrement gaps by 1px
	{ MODKEY|Mod1Mask,              XK_0,      togglegaps,     {0} },                // toggle gaps
	{ MODKEY|Mod1Mask|ShiftMask,    XK_0,      defaultgaps,    {0} },                // reset gaps

	// Launch applications
	{ MODKEY|ShiftMask,             XK_s,      spawn,          {.v = screencapcmd } },
	{ MODKEY,                       XK_e,      spawn,          {.v = fmcmd } },
	{ MODKEY,                       XK_f,      spawn,          {.v = browsercmd } },

	// Audio controls
//	{ 0,              XF86XK_AudioLowerVolume, spawn, {.v = voldowncmd   } },
//	{ 0,              XF86XK_AudioRaiseVolume, spawn, {.v = volupcmd     } },
//	{ 0,              XF86XK_AudioMute,        spawn, {.v = voltogglecmd } },

	// Media controls
	{ 0,                          MEDIAPLAY,   spawn,          {.v = mediaplaycmd } },
	{ 0,                          MEDIANEXT,   spawn,          {.v = medianextcmd } },
	{ 0,                          MEDIAPREV,   spawn,          {.v = mediaprevcmd } },

	// Brightness controls
	{ 0,                          BRIGHTUP,    spawn,          {.v = brightupcmd } },
	{ 0,                          BRIGHTDOWN,  spawn,          {.v = brightdowncmd } },

	// Tag keys (defined above)
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)

	// Quit dwm
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },                // quit dwm by stopping Xorg server
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};


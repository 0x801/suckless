#!/bin/bash

programs=( 'dmenu' 'dwm' 'slock' 'slstatus' 'st' )
dir=$(dirname "$0")

build_program()
{
	if [[ -d "$dir/$found" ]]; then
		cd "$dir/$found"
		rm -f config.h
		sudo make clean install -j$(nproc)
		rm -f config.h
	else
		echo "'$found' has no directory in '$dir'."
	fi
}

#=======
# MAIN
#=======
# List all suckless programs to build
if [[ $1 = 'list' ]]; then
	for p in ${programs[@]}; do
		echo $p
	done

# Handle given desktop option
else
	# A parameter must be given before parsing through the list of programs
	if [[ -n $1 ]]
	then
		found=''
		for p in ${programs[@]}; do
			if [[ $1 = $p ]]; then
				# Match found
				found=$p
				break
			fi
		done
	
		if [[ -z $found ]]; then
			echo "'$1' not recognized."
		else
			build_program
		fi
	else
		echo "Please enter a program to build."
	fi
fi

